import ovito
from ovito.data import *
from ovito.modifiers import *
from ovito.vis import *

import numpy as np
import copy

PRUNE_MAX = 1000

# this version is just here for historical reasons, trying to use a third-party algorithm for constructing the surface mesh
# this did not work very well!
# def create_mesh_pv(self, data: DataCollection):
#
#     #grab CIS atoms
#     cis_indices = np.where(np.logical_and(self.cis==1,self.selection))[0]
#     cis_points = np.array(self.positions[cis_indices])
#
#     #make an open3d surface mesh using the ball pivoting algorithm
#     pcd = o3d.geometry.PointCloud()
#     pcd.points = o3d.utility.Vector3dVector(cis_points)
#     pcd.estimate_normals()
#     radii = np.arange(1,40,0.1)  #[2,4,8,12]
#     surf = o3d.geometry.TriangleMesh.create_from_point_cloud_ball_pivoting(
#         pcd, o3d.utility.DoubleVector(radii))
#     # surf = o3d.geometry.TriangleMesh.create_from_point_cloud_alpha_shape(pcd, 0.03)
#
#     print(surf)
#     # #make a pyvista data object and then construct the surface mesh
#     # points = pv.wrap(cis_points)
#     # # surf = points.reconstruct_surface()
#     # surf = points.delaunay_2d()
#     #
#     # print(cis_indices)
#     # print(surf.faces.reshape(-1,4))
#
#
#
#     #make the mesh
#     mesh = data.surfaces.create('surface', title='surface', domain=data.cell)
#     # mesh = SurfaceMesh(domain=data.cell)
#     mesh.create_vertices(cis_points)
#     mesh.create_faces(surf.triangles)
#     # mesh.create_faces(surf.faces)
#     mesh.connect_opposite_halfedges()
#
#     #add the Index property so we know which atoms correspond to the vertices
#     mesh.vertices_.create_property('Index', data=cis_indices)
#
#     #set color to yellow, same as CIS atoms
#     # mesh.faces.create_property('Color', data=[(1,1,0)])
#
#     #misc properties
#     # mesh.vis.show_cap = False
#     # mesh.vis.surface_transparency = 1.0
#     # mesh.vis.highlight_edges = True
#     # mesh.vis.surface_color = (0, 0, 0)
    
    
    
def create_mesh_v2(self, data: DataCollection):
    """ 
    Create a surface mesh which only connects CIS atoms 
    The algorithm here is robust because all non-CIS atoms are systematically pruned from the surface mesh
    """
    yield "Constructing surface mesh..."

    #first select atoms to be used, only CIS atoms and atoms in Grain B
    #store current selection properties so they can be reset at the end
    selection0 = None
    if 'Selection' in data.particles:
        selection0 = data.particles['Selection'][:]
        data.particles_.selection_[:] = 0
    else:
        data.particles_.create_property('Selection',data=np.zeros(data.particles.count))


    #add CIS and grain B atoms (only those with the correct structure type)
    data.particles_.selection_[data.particles['Grain']==data.attributes['Grain B']] = 1
    data.particles_.selection_[self.structure_types!=self.g2_s] = 0
    data.particles_.selection_[data.particles['CIS']==1] = 1
    

    #construct the preliminary surface mesh
    data.particles_.create_property('Index', data = np.arange(data.particles.count))
    data.apply(ConstructSurfaceModifier(method = ConstructSurfaceModifier.Method.AlphaShape,
              radius = self.Rsphere, smoothing_level = 0,
              only_selected = True, map_particles_to_regions = True, transfer_properties=True))
    data.surfaces['surface'].vis.show_cap = False
    data.surfaces['surface'].vis.surface_transparency = 1.0
    data.surfaces['surface'].vis.highlight_edges = True
    data.surfaces['surface'].vis.surface_color = (0, 0, 0)
    
    #reset the original selection
    if selection0 is not None:
        data.particles_.selection_[:] = selection0
    else:
        data.apply(ClearSelectionModifier())

    #grab the mesh info
    mesh = data.surfaces['surface']
    # vertices = copy.deepcopy(np.array(mesh.vertices['Position']))
    # faces = copy.deepcopy(np.array(mesh.get_face_vertices()))
    # indices = copy.deepcopy(np.array(mesh.vertices['Index']))
    vertices = mesh.vertices['Position']
    faces = mesh.get_face_vertices()
    indices = mesh.vertices['Index']
    
    #now loop over the faces of the mesh and find faces that have one non-CIS vertex
    #adjust the mesh topology to remove these vertices from the mesh, and then remake the mesh
    #repeat this process until all non-CIS vertices are gone
    
    j=ind=prune_count=jmax=0
    yield "Pruning the surface mesh..."
    while True:
        #make a list of mesh faces containing only selected atoms
        selected_faces = np.nonzero(np.logical_and(self.selection[indices[faces[:,0]]]==1,
                                                   self.selection[indices[faces[:,1]]]==1,
                                                   self.selection[indices[faces[:,2]]]==1))[0]
        if prune_count==0:
            #number of non-CIS atoms to remove
            NnonCIS = np.count_nonzero(self.cis[indices[np.unique(np.ndarray.flatten(faces[selected_faces,:]))]]==0)
                                                   
        #find the first face containing exactly one non-CIS atom, keep track of which atom is non-CIS (ind variable)
        cis1 = self.cis[indices[faces[selected_faces,0]]]
        cis2 = self.cis[indices[faces[selected_faces,1]]]
        cis3 = self.cis[indices[faces[selected_faces,2]]]
        ind = 0
        nonCIS = np.nonzero(np.logical_and(np.logical_and(cis1==0,cis2==1),cis3==1))[0] 
        if len(nonCIS)==0:
            ind = 1
            nonCIS = np.nonzero(np.logical_and(np.logical_and(cis1==1,cis2==0),cis3==1))[0]
            if len(nonCIS)==0:
                ind = 2
                nonCIS = np.nonzero(np.logical_and(np.logical_and(cis1==1,cis2==1),cis3==0))[0]
                #none of the faces have non-CIS atoms so we're done!
                if len(nonCIS)==0:
                    break

        #use the first face in the list... (order doesn't matter)
        j = selected_faces[nonCIS[0]]     

        #vertex to be removed
        rm_vertex = faces[j,ind]
        
        #randomly choose one vertex as the pivot vertex
        pivot_vertex = faces[j,ind-1]          
        
        #delete both faces containing both the pivot vertex and rm_vertex
        faces = np.delete(faces,np.logical_and(np.any(faces==rm_vertex,axis=1),np.any(faces==pivot_vertex,axis=1)),axis=0)    
            
        #replace rm_vertex with the pivot vertex
        faces[faces==rm_vertex] = pivot_vertex

        #reset j and start again
        j = 0
        prune_count += 1

        #make sure we don't get too carried away pruning... abort if we do and throw a warning
        if prune_count>PRUNE_MAX:
            print("Warning: Number of surface mesh pruning events exceeded",PRUNE_MAX," - pruning was aborted")
            break
            #raise Exception("Number of surface mesh pruning events exceed 1e3 - something is probably wrong with the dataset")
        yield (prune_count/NnonCIS)

    print("Pruned",prune_count,"non-CIS atoms from the surface mesh.")

    #delete the old mesh
    del mesh
    del data.surfaces['surface']

    #remake the mesh with the changes so the topology gets updated
    mesh = data.surfaces.create('surface', title='surface', domain=data.cell)
    mesh.create_vertices(vertices)
    mesh.create_faces(faces)
    if not mesh.connect_opposite_halfedges():
        print("Warning: Unable to connect opposite half edges of the pruned surface mesh.")
    #add the Index property so we know which atoms correspond to the vertices
    mesh.vertices_.create_property('Index', data=indices)
    mesh.vertices_.create_property('Color', data=(1.0, 1.0, 0))
    
    #set the final display options        
    data.surfaces['surface'].vis.show_cap = False
    data.surfaces['surface'].vis.surface_transparency = 1.0
    data.surfaces['surface'].vis.highlight_edges = True
    data.surfaces['surface'].vis.surface_color = (1.0, 1.0, 0)

def create_mesh(self, data: DataCollection):
    """ 
    Create a surface mesh which only connects CIS atoms 
    The algorithm here does not always work (some non CIS atoms are used) in some cases
    """
    
    #first select atoms to be used, only CIS atoms and atoms in Grain B further than Rsphere/2 from any CIS atom
    #store current selection properties so they can be reset at the end
    selection0 = None
    if 'Selection' in data.particles:
        selection0 = data.particles['Selection'][:]
        data.particles_.selection_[:] = 0
    else:
        data.particles_.create_property('Selection',data=np.zeros(data.particles.count))
    
    
    #add CIS and grain B atoms
    data.particles_.selection_[data.particles['CIS']==1] = 1
    data.particles_.selection_[data.particles['Grain']==data.attributes['Grain B']] = 1
    
    #find all atoms within Rsphere/2 of CIS atoms using bonds        
    data.apply(CreateBondsModifier(cutoff=self.Rsphere))
    bond_topology = data.particles.bonds.topology
    a1= bond_topology[:,0]
    a2= bond_topology[:,1]
    nbrs = np.unique(np.concatenate((a1[(data.particles['CIS'][a1]==0) & (data.particles['CIS'][a2]==1)],
                      a2[(data.particles['CIS'][a1]==1) & (data.particles['CIS'][a2]==0)])))
    data.particles_.selection_[nbrs] = 0
    
    #delete all of the bonds
    data.particles_.bonds_.create_property('Selection',data=np.ones(data.particles.bonds.count))
    data.apply(DeleteSelectedModifier(operate_on={'bonds'}))
    
    #construct the surface mesh
    data.particles_.create_property('Index', data = np.arange(data.particles.count))
    data.apply(ConstructSurfaceModifier(method = ConstructSurfaceModifier.Method.AlphaShape,
             radius = self.Rsphere, smoothing_level = 0,
             only_selected = True, map_particles_to_regions = True, transfer_properties=True))
    data.surfaces['surface'].vis.show_cap = False
    data.surfaces['surface'].vis.surface_transparency = 1.0
    data.surfaces['surface'].vis.highlight_edges = True
    data.surfaces['surface'].vis.surface_color = (0, 0, 0)
    
    #make sure there aren't any non CIS atoms in the surface mesh
    # print((data.particles['CIS'][data.surfaces['surface'].vertices['Index']]))
#      if np.sum(data.particles['CIS'][data.surfaces['surface'].vertices['Index']]):
#          print("Error: Non coincidence sites in suface mesh - Rsphere needs to be increased")
                 
    #reset the original selection
    if selection0 is not None:
        data.particles_.selection_[:] = selection0
    else:
        data.apply(ClearSelectionModifier())