from ovito.data import *
from ovito.modifiers import *
import sys
import os

##########################
#Add your path to the src folder here!!!!!
sys.path.append("YOUR_PATH/src/")
##########################

import importlib
import main

#reload in case we edited it in real time
importlib.reload(main)

from main import ILDA

def modify(frame: int, data: DataCollection, selection_only=False, print_results=False, atomA=1, atomB=2, cis_tol=0.0, extract_lines=False, 
    aA=1.0, cA = 0.0, aB=1.0, cB = 0.0, typeA=-1, typeB=-1, Rsphere=10.0, btol=0.01, angtol=5.0, 
    distF=10.0, estimateF=True, xA=([1,0,0]), yA=([0,1,0]), xB=([1,0,0]), yB=([0,1,0]),
    EcohA=([0,0,0],[0,0,0],[0,0,0]), EcohB=([0,0,0],[0,0,0],[0,0,0])):
    
    #load the CoIncidenceSites class
    algo = ILDA(data,selection_only,print_results,atomA,atomB,cis_tol,extract_lines,aA,cA,aB,cB,typeA,typeB,Rsphere,btol,angtol,distF,estimateF,xA,yA,xB,yB,EcohA,EcohB) 
   
    #find the coincidence sites
    yield from algo.run(data)
